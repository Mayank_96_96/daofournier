// import $ from 'jquery';
import Instafeed from "../../assets/instafeed";

export default () => {
    ((custom, $) => {
        const $dom = {};

        const cacheDom = () => {
            $dom.bodyElement = $("body");
        };

        // Related Product
        const bindUIActions = () => {
            if ($("#back-to-top").length) {
                var scrollTrigger = 100, // px
                    backToTop = function () {
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop > scrollTrigger) {
                            $("#back-to-top").addClass("show");
                        } else {
                            $("#back-to-top").removeClass("show");
                        }
                    };
                backToTop();
                $(window).on("scroll", function () {
                    backToTop();
                });
                $("#back-to-top").on("click", function (e) {
                    e.preventDefault();
                    $("html,body").animate({
                            scrollTop: 0
                        },
                        700
                    );
                });
            }
        };

        $(".close").click(function (e) {
            $(".prod-slid").empty();
            $(".prod-slid").removeClass(
                "slick-initialized",
                "slick-slider",
                "slick-dotted"
            );
            $("#product-title").empty();
            $("#select-var").empty();
            $("#product-price").empty();
        });
        $(".btn-quick_View").click(function (e) {
            var data_pro = $(this).data("des");
            $.ajax({
                url: data_pro + ".js",
                type: "GET",
                success: function (res) {
                    var data = JSON.parse(res);
                    var price = 0;
                    var original_price = 0;
                    var options = data.options;
                    $("#product-title").text(data.title);
                    $(data.variants).each(function (i, v) {
                        price = parseInt(v.price / 100).toFixed(2);
                        original_price = parseInt(v.compare_at_price / 100).toFixed(2);
                        $("#product-price").text("$." + price);
                    });
                    
                    $("#p_des").html(data.description);
                    $(".pro_id").val(data.variants[0].id);
                    $(options).each(function (i, option) {
                        var opt = option.name;
                        var selectClass = ".option." + opt.toLowerCase();
                        $("#select-var").append(
                            '<div class="option-selection-' +
                            opt.toLowerCase() +
                            '"><select class="form-control form_txt option-' +
                            i +
                            " option " +
                            opt.toLowerCase() +
                            '"></select></div>'
                        );
                        $(option.values).each(function (i, value) {
                            $(".option." + opt.toLowerCase()).append(
                                '<option value="' + value + '">' + value + "</option>"
                            );
                        });
                    });

                    var img = data.images;
                    $(img).each(function (i, image) {
                        var image_embed = '<div class=image_box ><img src="' + image + '"></div>';
                        image_embed = image_embed
                            .replace(".jpg", "_1024x.jpg")
                            .replace(".png", "_1024x.png");
                        $(".prod-slid").append(image_embed);
                        setTimeout(function () {
                            $(".prod-slid").slick({
                                dots: true,
                                arrows: true,
                                respondTo: "min",
                                useTransform: false
                            });
                        }, 500);
                    });
                    $(data.variants).each(function (i, variant) {
                        if (variant.title == "Default Title") {
                            $("#select-var").hide();
                        } else {
                            $("#select-var").show();
                        }
                    });
                }
            });
        });

        $(".read-more-btn").click(function (event) {
            event.preventDefault();
            var description = document.querySelector(".product__description");
            console.log(description.style.height);
            if (description.style.height === "") {
                description.style.height = "auto";
            } else if (description.style.height === "auto") {
                description.style.height = "";
            } else {
                description.style.height = "92px";
            }

            $(this).text($(this).text() == "Read less" ? "Read more" : "Read less");
        });

        $(".cart-btn").click(function () {
            $(".sidenav").animate({
                right: "0px"
            });
        });

        $(".closebtn").click(function () {
            $(".sidenav").animate({
                right: "-100%"
            });
        });

        $(".menu-toogle-btn").click(function () {
            $(".mobile-menu-div").css({
                left: "0%"
            });
        });
        $(".close-btn").click(function () {
            $(".mobile-menu-div").css({
                left: "-100%"
            });
        });
        $(".mob-nav-ul li a").click(function () {
            $(this)
                .next()
                .removeClass("close-sub");
            $(this)
                .next()
                .addClass("open-sub");
        });
        $(".head-top").click(function () {
            $(".sub-menu").removeClass("open-sub");
            $(".sub-menu").addClass("close-sub");
        });
        $(
            ".megamenu-tabs .tab-pane:eq(0), .categori-ul li:eq(0) .menmenu-tab-controller"
        ).addClass("active");

        $(".menmenu-tab-controller").on("click", function (event) {
            event.preventDefault();
            window.location.href = $(this).data("href");
        });

        $(".mob-cat-li a").click(function () {
            $(this).next().slideToggle();
        });

        //form login
        $("#forget").click(function () {
            $(".lgn").hide();
            $("#recover").show();
        });
        $(".cnl").click(function () {
            $("#recover").hide();
            $(".lgn").show();
        });

        // PDP Qty Selector
        $("button.qty-btns").on("click", function (event) {
            event.preventDefault();
            var container = $(event.currentTarget).closest("[data-qtyContainer]");
            var qtEle = $(container).find("[data-qty]");
            var currentQty = $(qtEle).val();
            var qtyDirection = $(this).data("direction");
            var newQty = 0;

            if (qtyDirection == "1") {
                newQty = parseInt(currentQty) + 1;
            } else if (qtyDirection == "-1") {
                newQty = parseInt(currentQty) - 1;
            }

            if (newQty == 1) {
                $(".decrement-quantity").attr("disabled", "disabled");
            }
            if (newQty > 1) {
                $(".decrement-quantity").removeAttr("disabled");
            }

            if (newQty > 0) {
                newQty = newQty.toString();
                $(qtEle).val(newQty);
            } else {
                $(qtEle).val("1");
            }
        });

        custom.init = () => {
            cacheDom();
            bindUIActions();
        };
    })((window.custom = window.custom || {}), $);
};
jQuery(document).ready(function () {
    $('#ex1').zoom();
    // This button will increment the value
    $(".qtyplus").click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get its current value
        var currentVal = parseInt(
            $(this)
            .closest(".qnt_txt")
            .find(".product__qty")
            .val()
        );
        var item = $(this).data("item-id");
        currentVal++;
        changeItem(currentVal, item);
        $(this)
            .closest(".qnt_txt")
            .find(".product__qty")
            .val(currentVal);
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        // Get its current value
        var item = $(this).data("item-id");
        var currentVal = parseInt(
            $(this)
            .closest(".qnt_txt")
            .find(".product__qty")
            .val()
        );
        if (currentVal == 1) {} else {
            currentVal--;
            changeItem(currentVal, item);
            $(this)
                .closest(".qnt_txt")
                .find(".product__qty")
                .val(currentVal);
        }
    });

    function changeItem(quantity, item) {
        $.ajax({
            type: "POST",
            url: "/cart/change.js",
            data: {
                id: item,
                quantity: quantity
            },
            dataType: "json",
            success: function (cart) {
                $(this)
                    .closest(".qnt_txt")
                    .find(".product__qty")
                    .val(quantity);
                var price = cart.total_price;
                price = parseInt(price / 100).toFixed(2);
                $(".total-price").text("$. " + price);
                $(this)
                    .closest(".pr-div")
                    .find(".nex-div")
                    .text(price);

                var cart_items = cart.items;
                $(cart_items).each(function (i, cart_data) {
                    if (cart_data.id === item) {
                        var price = cart_data.line_price;
                        price = parseInt(price / 100).toFixed(2);
                        $(".pro-price-" + item).text("$. " + price);
                    }
                });
                console.log("Done");
            },
            error: function () {
                console.log("Fail");
            }
        });
    }

    // Instagram
    var User_Id = $("#instafeed").data("user_id");
    var Access_Token = $("#instafeed").data("access_token");
    var limit = $("#instafeed").data("limit");
    var userFeed = new Instafeed({
        get: "user",
        userId: User_Id,
        accessToken: Access_Token,
        limit: limit,
        resolution: "standard_resolution",
        template: '<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4 py-3"><div class="product-img"><div class="product-list"></div><a href="{{link}}" target="_blank"><img  src="{{image}}"></a></div></div></div>'
    });
    userFeed.run();

    $("#forget").click(function () {
        if ($("#recover").hasClass("d-none")) {
            $("#recover").removeClass("d-none");
            $("#login-form").addClass("d-none");
        }
    });
    $("#cancel").click(function () {
        if ($("#login-form").hasClass("d-none")) {
            $("#login-form").removeClass("d-none");
            $("#recover").addClass("d-none");
        }
    });

    $(".cart-btn").click(function () {
        $(".quick-cart-footer ").removeClass("close");
        $(".quick-cart-footer ").addClass("open");
    });
    $(".closebtn").click(function () {
        $(".quick-cart-footer ").removeClass("open");
        $(".quick-cart-footer ").addClass("close");
    });

    $(".bg-img").each(function (i, data) {
        var bg_color = $(this).find(".bg-variant").data("img");
        var bg_var = $(this).find(".bg-variant");
        if (bg_color == 'gold finish') {
            $(bg_var).css("background-image", "url(" + 'https://cdn.shopify.com/s/files/1/0286/7147/5765/files/gold-finish-swatch_001.png?v=1577083028' + ")");
        } else if (bg_color == 'rose gold finish') {
            $(bg_var).css("background-image", "url(" + 'https://cdn.shopify.com/s/files/1/0286/7147/5765/files/gold-finish-swatch.png?v=1577083028' + ")");
        } else {
            $(bg_var).css("background-image", "url(" + 'https://cdn.shopify.com/s/files/1/0286/7147/5765/files/silver-finish-swatch.png?v=1577083028' + ")");
        }
    });

    $("#product-select-new").click(function () {
        var value = $("#product-select-new").find(".active").data("value");
        $('#product-select').find('option[value="' + value + '"]').prop('selected', 'selected');
    });

    $(".cart-add").click(function () {
        cartDrawer.offcanvas("open");
    });
});