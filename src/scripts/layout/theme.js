import "bootstrap";
import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';

import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';

import "../../styles/slick.scss";
import "../../styles/slick-theme.scss";

window.$ = window.jQuery = $;
import "bootstrap";

// Custom Js
import custom from "./custome";
import Newsletter from '../modules/newsletter';
import slider from '../modules/slider';
import sticky from '../modules/sticky';
import loadMore from '../modules/loadMore';

// import quickViewaction from "../modules/quickview";

sticky();
custom();
Newsletter();
slider();
loadMore();
// animate();
// quickViewaction();

window.sticky.init();
window.custom.init();
// window.animate.init();
window.Newsletter.init();
window.Slider.init();
// window.QuickView.init();

// import "./animate";