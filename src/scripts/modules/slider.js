import slick from "slick-carousel";

export default () => {
  (function (Slider, $) {
    const $dom = {};
    const arrow =
      '<svg fill="#959595" viewBox="0 0 129 129"><path d="M121.3 34.6c-1.6-1.6-4.2-1.6-5.8 0l-51 51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8 0-1.6 1.6-1.6 4.2 0 5.8l53.9 53.9c.8.8 1.8 1.2 2.9 1.2 1 0 2.1-.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2.1-5.8z"/></svg>';
    const cacheDom = () => {
      $dom.pdpSliderVar = $(".product-sliderVar");
      $dom.bannerSlider = $(".banner-slide");
      $dom.testimonialSlider = $(".testimonial-slide");
      $dom.pdpSlider = $(".product-slider");
      $dom.relatedSlider = $(".related-product-slider");
      $dom.formulaSlider = $(".proSlider");
      $dom.proSlider = $(".in-pro-slider");
      $dom.proSlider1 = $(".slid-img");

      $dom.pdpSliderindex = $(".img-slider");
      $dom.pdpSliderFor = $(".pro-nav-ul");
    };

    const SlidersInitindex = () => {
      const pdpSliderOptions = {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        asNavFor: $dom.pdpSliderFor
      }

      const pdpSliderSliderOptions = {
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: $dom.pdpSliderindex,
        dots: false,
        vertical: false,
        centerMode: true,
        arrows: false,
        focusOnSelect: true,
        fade: true
      }

      $dom.pdpSliderindex.slick(pdpSliderOptions);
      $dom.pdpSliderFor.slick(pdpSliderSliderOptions);
    };

    const SlidersInitVar = () => {
      const pdpSliderOptionsvar = {
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        vertical: true
      };

      $dom.pdpSliderVar.slick(pdpSliderOptionsvar);
    };

    const proSlidersInit = () => {
      const proSliderOptions = {
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        nav: false,
        arrows: false,
        responsive: [{
            breakpoint: 992,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              slidesToShow: 1,
              autoplay: true
            }
          }
        ]
      };
      $dom.proSlider.slick(proSliderOptions);
    };

    const proSlider1sInit = () => {
      const proSlider1Options = {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        nav: false,
        arrows: false,
        dots: true
      };
      $dom.proSlider1.slick(proSlider1Options);
    };

    const bannerSlidersInit = () => {
      const bannerSliderOptions = {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        nav: false,
        arrows: false
      };
      $dom.bannerSlider.slick(bannerSliderOptions);
    };

    const testimonialSlidersInit = () => {
      const testimonialSliderOptions = {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        nav: false,
        arrows: true,
        responsive: [{
          breakpoint: 768,
          settings: {
            arrows: false
          }
        }]
      };
      $dom.testimonialSlider.slick(testimonialSliderOptions);
    };

    const SlidersInit = () => {
      const pdpSliderOptions = {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true
      };

      $dom.pdpSlider.slick(pdpSliderOptions);
    };

    const relatedSlidersInit = () => {
      const relatedSliderOptions = {
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        nav: false,
        arrows: true,
        responsive: [{
            breakpoint: 992,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 767,
            settings: {
              arrows: false,
              slidesToShow: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              slidesToShow: 1
            }
          }
        ]
      };
      $dom.relatedSlider.slick(relatedSliderOptions);
    };

    const formulaSlidersInit = () => {
      const formulaSliderOptions = {
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        nav: false,
        arrows: false,
        responsive: [{
            breakpoint: 992,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              autoplay: true,
              slidesToShow: 1
            }
          }
        ]
      };
      $dom.formulaSlider.slick(formulaSliderOptions);
    };

    Slider.init = function () {
      cacheDom();
      SlidersInitVar();
      proSlidersInit();
      bannerSlidersInit();
      testimonialSlidersInit();
      SlidersInit();
      relatedSlidersInit();
      formulaSlidersInit();
      proSlider1sInit();
      SlidersInitindex();
    };
  })((window.Slider = window.Slider || {}), $, undefined);
};