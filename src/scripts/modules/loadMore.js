// import $ from 'jquery';

export default () => {
    ((loadmore, $) => {
        'use strict';
        let triggered = false;
        const $dom = {};

        const cacheDom = () => {
            $dom.bodyElement = $('.btn-show-more');
        };

        const ScrollExecute = () => {
            // Locate loadmore button
            let moreButon = $('#more').last();
            // Get URL from the loadmore button
            let nextUrl = $(moreButon).find('a').attr("href");

            // Button position when AJAX call should be made one time
            if ((($(moreButon).offset().top - $(window).scrollTop()) < 800) && (triggered == false)) {

                // Trigger shortcircuit to ensure AJAX only fires once
                triggered = true;

                // Make ajax call to next page for load more data
                $.ajax({
                        url: nextUrl,
                        type: 'GET',
                        beforeSend: function () {
                            moreButon.remove();
                        }
                    })
                    .done(function (data) {
                        // Append data
                        $('.product').append($(data).find('.product').html());

                        // On success, reset shortcircuit
                        triggered = false
                    });
            }
        };

        const bindUIActions = () => {
            let collectionPage = document.querySelector('.template-collection');
            if(collectionPage){
                $('body').on('click', '.btn-show-more', function(e){
                    e.preventDefault();
                    ScrollExecute();
                })
            }
        };
        Loadmore.init = () => {
            cacheDom();
            bindUIActions();
        };

    })(window.Loadmore = window.Loadmore || {}, $);
}